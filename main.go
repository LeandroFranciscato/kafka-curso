package main

import (
	"kafka/config"
	example "kafka/examples"
)

func main() {
	config.Load()

	//example.Producer1()
	//example.Consumer1()
	example.Transactions()
}
