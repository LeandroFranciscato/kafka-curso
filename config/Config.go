package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var (
	AppID            string
	BootstrapServers string
	TopicName        string
)

// Load carregas as variáveis de ambiente contidas no .env, na raiz do Projeto
func Load() {
	var erro error = godotenv.Load()
	if erro != nil {
		log.Fatal(erro)
	}

	AppID = os.Getenv("APP_ID")
	BootstrapServers = os.Getenv("BOOTSTRAP_SERVERS")
	TopicName = os.Getenv("TOPIC_NAME")
}
