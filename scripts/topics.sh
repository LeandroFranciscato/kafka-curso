#!/bin/sh

cd /home/leandro/kafka

#create
bin/kafka-topics.sh --create --zookeeper localhost:2181 --topic topico1 --partitions 5 --replication-factor 3

#delete
bin/kafka-topics.sh --delete --zookeeper localhost:2181 --topic topico1

#describe
bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic topico1

#open zookeeper shell
bin/zookeeper-shell.sh localhost:2181

#cosumer console
bin/kafka-console-consumer.sh --from-beginning --bootstrap-server localhost:9093 --topic topico3
bin/kafka-console-consumer.sh --from-beginning --bootstrap-server localhost:9093 --whitelist "topico2|topico3"