package example

import (
	"context"
	"encoding/json"
	"fmt"
	"kafka/config"
	"log"
	"strconv"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type Pessoa struct {
	ID    int    `json:"id"`
	Nome  string `json:"nome"`
	Email string `json:"email"`
}

func Producer1() {
	log.Println("Inicio do programa")

	configs := kafka.ConfigMap{
		"bootstrap.servers": config.BootstrapServers,
	}

	producer, err := kafka.NewProducer(&configs)
	if err != nil {
		log.Fatal(err)
	}
	defer producer.Close()

	// Producer handle function to delivered messages
	go func() {
		for event := range producer.Events() {
			switch ev := event.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
				} else {
					fmt.Printf("Delivery Succeed: %v\n", ev.TopicPartition)
				}
			}
		}
	}()

	pessoa := Pessoa{1, "lendroelditado", "leandro@ingaflex.com.breditado"}
	pessoaByte, err := json.Marshal(pessoa)
	if err != nil {
		log.Fatal(err)
	}

	topic := config.TopicName
	//for i := 0; i < 1000000; i++ {
	msg := kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Key:            []byte(strconv.Itoa(pessoa.ID)),
		Value:          pessoaByte,
	}
	producer.Produce(&msg, nil)
	//}

	producer.Flush(5 * 1000)
}

func Consumer1() {
	configs := kafka.ConfigMap{
		"bootstrap.servers": config.BootstrapServers,
	}

	consumer, err := kafka.NewConsumer(&configs)
	if err != nil {
		log.Fatal(err)
	}
	defer consumer.Close()

	consumer.SubscribeTopics([]string{"hello-producer-topic"}, nil)
	msg, err := consumer.ReadMessage(-1)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(msg)

}

func Transactions() {

	events := 2
	topico1 := "topico2"
	topico2 := "topico3"
	transactionID := "transaction"

	configs := kafka.ConfigMap{
		"bootstrap.servers": config.BootstrapServers,
		"transactional.id":  transactionID,
	}

	producer, err := kafka.NewProducer(&configs)
	if err != nil {
		log.Fatal(err)
	}
	defer producer.Close()

	producer.InitTransactions(context.TODO())

	producer.BeginTransaction()
	for i := 0; i < events; i++ {
		msg := kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topico1, Partition: kafka.PartitionAny},
			Value:          []byte("teste"),
		}
		producer.Produce(&msg, nil)

		msg2 := kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topico2, Partition: kafka.PartitionAny},
			Value:          []byte("teste"),
		}
		producer.Produce(&msg2, nil)
	}
	producer.CommitTransaction(context.TODO())

	// next will rollback
	producer.BeginTransaction()
	for i := 0; i < events; i++ {
		msg := kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topico1, Partition: kafka.PartitionAny},
			Value:          []byte("teste2222"),
		}
		producer.Produce(&msg, nil)

		msg2 := kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topico2, Partition: kafka.PartitionAny},
			Value:          []byte("teste2222"),
		}
		producer.Produce(&msg2, nil)
	}
	producer.AbortTransaction(context.TODO())

}
